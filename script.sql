CREATE SCHEMA `shop` ;

-- -----------------------------------------------------
-- Table `shop`.`User`
-- -------------------------------------shopshopshopshopshopshop----------------
CREATE TABLE IF NOT EXISTS `shop`.`User` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(150) NULL,
  `Surname` VARCHAR(150) NULL,
  `Phone` VARCHAR(300) NULL,
  `Email` VARCHAR(150) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`Order` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Address` VARCHAR(300) NULL,
  `Created` DATETIME NULL,
  `Status` INT NULL,
  `User_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Order_User_idx` (`User_Id` ASC),
  CONSTRAINT `fk_Order_User`
    FOREIGN KEY (`User_Id`)
    REFERENCES `shop`.`User` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`Category` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NULL,
  `Image` VARCHAR(45) NULL,
  `Active` TINYINT NULL,
  `Created` DATETIME NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`Product` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NULL,
  `Description` VARCHAR(500) NULL,
  `Code` VARCHAR(45) NULL,
  `Views` BIGINT NULL,
  `Active` TINYINT NULL,
  `Created` DATETIME NULL,
  `Updated` DATETIME NULL,
  `Category_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Product_Category1_idx` (`Category_Id` ASC),
  CONSTRAINT `fk_Product_Category1`
    FOREIGN KEY (`Category_Id`)
    REFERENCES `shop`.`Category` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`Order_has_Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`Order_has_Product` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Order_Id` INT NOT NULL,
  `Product_Id` INT NOT NULL,
   PRIMARY KEY (`Id`),
  INDEX `fk_Order_has_Product_Product1_idx` (`Product_Id` ASC),
  INDEX `fk_Order_has_Product_Order1_idx` (`Order_Id` ASC),
  CONSTRAINT `fk_Order_has_Product_Order1`
    FOREIGN KEY (`Order_Id`)
    REFERENCES `shop`.`Order` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_has_Product_Product1`
    FOREIGN KEY (`Product_Id`)
    REFERENCES `shop`.`Product` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


ALTER TABLE `shop`.`Product` ADD Price Double NULL; 

ALTER TABLE `shop`.`product` 
ADD COLUMN `Image` VARCHAR(300) NULL AFTER `Price`,
ADD COLUMN `ImageBig` VARCHAR(300) NULL DEFAULT NULL AFTER `Image`;

ALTER TABLE `shop`.`product` 
CHANGE COLUMN `Description` `Description` VARCHAR(1000) NULL DEFAULT NULL ;

ALTER TABLE `shop`.`order_has_product` RENAME TO  `shop`.`order_products` ;
ALTER TABLE `shop`.`order_products` RENAME TO  `shop`.`order_product` ;

ALTER TABLE `shop`.`order_product` ADD COLUMN `Count` INT NOT NULL;

CREATE TABLE `shop`.`account` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Phone` VARCHAR(300) NOT NULL,
  `Password` VARCHAR(1000) NOT NULL,
  `Role` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`Id`));

-- -----------------------------------------------------
-- ROWS
-- -----------------------------------------------------
insert into account(Phone, Password, Role) values("0675184805", "1111", "admin");


insert into shop.category(Name, Active, Created) values('Roses', 1, NOW());
insert into shop.category(Name, Active, Created) values('Tulips', 1, NOW());
insert into shop.category(Name, Active, Created) values('Orchids', 1, NOW());
insert into shop.category(Name, Active, Created) values('Сhrysanthemum', 1, NOW());


insert into shop.product(Name, Description, Code, Views, Active, Created, Updated, Category_Id, Price, Image, ImageBig)
values("Ut eu feugiat",
"Sed ullamcorper nunc at magna egestas fermentum. Etiam turpis orci, condimentum luctus orci id, elementum vulputate nunc. Donec diam turpis, iaculis vitae feugiat ac, molestie at odio. Nullam tincidunt est ac sagittis ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper quam sit amet turpis rhoncus id venenatis tellus sollicitudin. Fusce ullamcorper, dolor non mollis pulvinar, turpis tortor commodo nisl, et semper lectus augue blandit tellus. Quisque id bibendum libero.",
"BN 1111", 0, 1, NOW(), NOW(), 1, 240,
"images/product/01.jpg",
"images/product/01_l.jpg");

insert into shop.product(Name, Description, Code, Views, Active, Created, Updated, Category_Id, Price, Image, ImageBig)
values("UDonec Est Nisi",
"Sed ullamcorper nunc at magna egestas fermentum. Etiam turpis orci, condimentum luctus orci id, elementum vulputate nunc. Donec diam turpis, iaculis vitae feugiat ac, molestie at odio. Nullam tincidunt est ac sagittis ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper quam sit amet turpis rhoncus id venenatis tellus sollicitudin. Fusce ullamcorper, dolor non mollis pulvinar, turpis tortor commodo nisl, et semper lectus augue blandit tellus. Quisque id bibendum libero.",
"BN 1112", 0, 1, NOW(), NOW(), 4, 160,
"images/product/02.jpg",
"images/product/02_l.jpg");

insert into shop.product(Name, Description, Code, Views, Active, Created, Updated, Category_Id, Price, Image, ImageBig)
values("Tristique Vitae",
"Sed ullamcorper nunc at magna egestas fermentum. Etiam turpis orci, condimentum luctus orci id, elementum vulputate nunc. Donec diam turpis, iaculis vitae feugiat ac, molestie at odio. Nullam tincidunt est ac sagittis ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper quam sit amet turpis rhoncus id venenatis tellus sollicitudin. Fusce ullamcorper, dolor non mollis pulvinar, turpis tortor commodo nisl, et semper lectus augue blandit tellus. Quisque id bibendum libero.",
"BN 1113", 0, 1, NOW(), NOW(), 1, 140,
"images/product/03.jpg",
"images/product/03_l.jpg");

insert into shop.product(Name, Description, Code, Views, Active, Created, Updated, Category_Id, Price, Image, ImageBig)
values("Hendrerit Eu",
"Sed ullamcorper nunc at magna egestas fermentum. Etiam turpis orci, condimentum luctus orci id, elementum vulputate nunc. Donec diam turpis, iaculis vitae feugiat ac, molestie at odio. Nullam tincidunt est ac sagittis ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper quam sit amet turpis rhoncus id venenatis tellus sollicitudin. Fusce ullamcorper, dolor non mollis pulvinar, turpis tortor commodo nisl, et semper lectus augue blandit tellus. Quisque id bibendum libero.",
"BN 1114", 0, 1, NOW(), NOW(), 1, 320,
"images/product/04.jpg",
"images/product/04_l.jpg");

insert into shop.product(Name, Description, Code, Views, Active, Created, Updated, Category_Id, Price, Image, ImageBig)
values("Tincidunt Nisi",
"Sed ullamcorper nunc at magna egestas fermentum. Etiam turpis orci, condimentum luctus orci id, elementum vulputate nunc. Donec diam turpis, iaculis vitae feugiat ac, molestie at odio. Nullam tincidunt est ac sagittis ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper quam sit amet turpis rhoncus id venenatis tellus sollicitudin. Fusce ullamcorper, dolor non mollis pulvinar, turpis tortor commodo nisl, et semper lectus augue blandit tellus. Quisque id bibendum libero.",
"BN 1115", 0, 1, NOW(), NOW(), 3, 150,
"images/product/05.jpg",
"images/product/05_l.jpg");

insert into shop.product(Name, Description, Code, Views, Active, Created, Updated, Category_Id, Price, Image, ImageBig)
values("Curabitur et turpis",
"Sed ullamcorper nunc at magna egestas fermentum. Etiam turpis orci, condimentum luctus orci id, elementum vulputate nunc. Donec diam turpis, iaculis vitae feugiat ac, molestie at odio. Nullam tincidunt est ac sagittis ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper quam sit amet turpis rhoncus id venenatis tellus sollicitudin. Fusce ullamcorper, dolor non mollis pulvinar, turpis tortor commodo nisl, et semper lectus augue blandit tellus. Quisque id bibendum libero.",
"BN 1116", 0, 1, NOW(), NOW(), 2, 110,
"images/product/06.jpg",
"images/product/06_l.jpg");

insert into shop.product(Name, Description, Code, Views, Active, Created, Updated, Category_Id, Price, Image, ImageBig)
values("Mauris consectetur",
"Sed ullamcorper nunc at magna egestas fermentum. Etiam turpis orci, condimentum luctus orci id, elementum vulputate nunc. Donec diam turpis, iaculis vitae feugiat ac, molestie at odio. Nullam tincidunt est ac sagittis ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper quam sit amet turpis rhoncus id venenatis tellus sollicitudin. Fusce ullamcorper, dolor non mollis pulvinar, turpis tortor commodo nisl, et semper lectus augue blandit tellus. Quisque id bibendum libero.",
"BN 1117", 0, 1, NOW(), NOW(), 3, 130,
"images/product/07.jpg",
"images/product/07_l.jpg");

insert into shop.product(Name, Description, Code, Views, Active, Created, Updated, Category_Id, Price, Image, ImageBig)
values("Proin volutpat",
"Sed ullamcorper nunc at magna egestas fermentum. Etiam turpis orci, condimentum luctus orci id, elementum vulputate nunc. Donec diam turpis, iaculis vitae feugiat ac, molestie at odio. Nullam tincidunt est ac sagittis ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper quam sit amet turpis rhoncus id venenatis tellus sollicitudin. Fusce ullamcorper, dolor non mollis pulvinar, turpis tortor commodo nisl, et semper lectus augue blandit tellus. Quisque id bibendum libero.",
"BN 1118", 0, 1, NOW(), NOW(), 2, 170,
"images/product/08.jpg",
"images/product/08_l.jpg");




ALTER TABLE `shop`.`product` 
CHANGE COLUMN `ImageBig` `Image_Big` VARCHAR(300) NULL DEFAULT NULL ;




