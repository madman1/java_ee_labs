package com.store.request;

import com.store.entities.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class UserResponse
{
    private Integer id;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private String role;

    public UserResponse(User user, String role)
    {
        this.id = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.role = role;
    }
}
