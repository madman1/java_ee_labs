package com.store.request;

import com.store.entities.Category;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@NoArgsConstructor
@EqualsAndHashCode
public class ProductRequest {
    private String name;
    private String description;
    private Boolean active;
    private Double price;
    private Integer categoryID;
}
