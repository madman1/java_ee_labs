package com.store.request;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class OrderRequest
{
    private String name;
    private String surname;
    private String address;
    private String email;
    private String phone;
    private List<ProductInfo> products;

    public OrderRequest()
    {
        products = new ArrayList<ProductInfo>();
    }
}
