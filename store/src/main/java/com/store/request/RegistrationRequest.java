package com.store.request;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class RegistrationRequest
{
    private String email;
    private String phone;
    private String surname;
    private String name;
    private String password;
    private String address;
    private String role;
}
