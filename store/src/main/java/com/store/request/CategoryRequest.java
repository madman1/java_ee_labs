package com.store.request;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.sql.Timestamp;

@Getter
@NoArgsConstructor
@EqualsAndHashCode
public class CategoryRequest
{
    private String name;
    private String image;
    private Boolean active;
}
