package com.store.request;

import com.store.entities.Order;
import com.store.entities.OrderStatus;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class OrderResponse
{
    private Integer id;
    private String name;
    private String surname;
    private String address;
    private String email;
    private String phone;
    private String status;
    private String date;

    public OrderResponse()
    {
    }

    public OrderResponse(Integer id,
                         String name,
                         String surname,
                         String address,
                         String email,
                         String phone,
                         int status,
                         Date date)
    {
        this.id = id;
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.surname = surname;
        this.status = status == 0 ?
                OrderStatus.ORDERED.toString() : status == 1 ?
                OrderStatus.SENDED.toString() : OrderStatus.FINISHED.toString();
        this.date = date.toString();
    }
}
