package com.store.repository;

import com.store.entities.Category;
import com.store.entities.OrderProduct;
import com.store.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderProductRepository extends JpaRepository<OrderProduct, Integer> {
}