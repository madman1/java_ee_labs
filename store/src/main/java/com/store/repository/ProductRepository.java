package com.store.repository;

import com.store.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    @Query("select p from Product p where p.active = 1")
    public List<Product> findAllByActive();

    @Query("select p from Product p where p.active = 1 and p.category.id = ?1")
    public List<Product> findByCategoryId(Integer categoryId);

    /*@Query("select p from Product p where p.category.id in (?1)")
    public List<Product> findByIds(List<Integer> categoryId);*/
}
