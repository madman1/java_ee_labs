package com.store.repository;

import com.store.entities.Category;
import com.store.entities.Order;
import com.store.entities.User;
import com.store.entities.OrderProduct;
import com.store.entities.Product;
import com.store.request.OrderResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer>
{
    @Query("select new com.store.request.OrderResponse(t1.id, t2.name, t2.surname, t1.address, t2.email, t2.phone, t1.status, t1.created)" +
            " from Order t1 inner join t1.user t2  where t1.user.phone = ?1")
    public List<OrderResponse> findByUserPhone(String userPhone);

    @Query("select new com.store.request.OrderResponse(t1.id, t2.name, t2.surname, t1.address, t2.email, t2.phone, t1.status, t1.created)" +
            " from Order t1 inner join t1.user t2")
    public List<OrderResponse> findAllConcated();
}
