package com.store.repository;

import com.store.entities.Account;
import com.store.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface AccountRepository extends JpaRepository<Account, Integer>
{
    @Query("select p from Account p where p.phone = ?1")
    public Account findByPhone(String phone);
}