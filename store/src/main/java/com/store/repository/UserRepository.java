package com.store.repository;

import com.store.entities.Product;
import com.store.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer>  {

    @Query("select p from User p where p.phone = ?1")
    public User findByPhone(String phone);
}
