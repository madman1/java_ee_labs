package com.store.controllers;

import com.store.entities.Category;
import com.store.repository.CategoryRepository;
import com.store.request.CategoryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@CrossOrigin(origins = "*")
@Controller
public class CategoryRestController
{
    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping(value = "/api/category", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Category> getAll()
    {
        return categoryRepository.findAll();
    }

    @RequestMapping(value = "/api/category/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<?> deleteById(@PathVariable int id)
    {
        categoryRepository.delete(id);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/api/category", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createNew(@RequestBody CategoryRequest newCategory)
    {
        Category res = new Category();
        res.setActive(newCategory.getActive()? (byte)1 : (byte)0);
        res.setCreated(new Timestamp(System.currentTimeMillis()));
        res.setImage(newCategory.getImage());
        res.setName(newCategory.getName());
        categoryRepository.save(res);
        return ResponseEntity.ok(null);
    }
}
