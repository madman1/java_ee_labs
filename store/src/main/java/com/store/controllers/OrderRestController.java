package com.store.controllers;

import com.store.entities.*;
import com.store.repository.OrderProductRepository;
import com.store.repository.OrderRepository;
import com.store.repository.ProductRepository;
import com.store.repository.UserRepository;
import com.store.request.OrderRequest;
import com.store.request.OrderResponse;
import com.store.request.ProductInfo;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@CrossOrigin(origins = "*")
@Controller
public class OrderRestController {
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final OrderProductRepository orderProductRepository;

    @Autowired
    public OrderRestController(OrderRepository orderRepository, UserRepository userRepository, ProductRepository productRepository, OrderProductRepository orderProductRepository) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.orderProductRepository = orderProductRepository;
    }

    @RequestMapping(value = "/api/secured/order", method = RequestMethod.GET)
    public ResponseEntity<?> getOrders(HttpServletRequest request)
    {
        Claims claims = (Claims) request.getAttribute("claims");
        String phoneNumber = claims.get("sub").toString();
        String role = claims.get("role").toString();

        OrderResponse response = new OrderResponse();

        List<OrderResponse> orders = null;

        if(role.equals("admin"))
        {
            orders = orderRepository.findAllConcated();
        }
        else
        {
            orders = orderRepository.findByUserPhone(phoneNumber);
        }

        return ResponseEntity
                .status(HttpStatus.CREATED)
            .body(orders);
    }

    @RequestMapping(value = "/api/order", method = RequestMethod.POST)
    public ResponseEntity createNewOrder(@RequestBody OrderRequest orderRequest)
    {
        if(orderRequest.getName() == null || orderRequest.getPhone() == null || orderRequest.getProducts().size() == 0)
        {
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("Request parameters are incorrect");
        }
        List<Integer> productIds = orderRequest.getProducts().stream().map(x-> x.getId()).collect(Collectors.toList());
        List<Product> products = productRepository.findAll(productIds);

        User user = userRepository.findByPhone(orderRequest.getPhone());
        if(user == null)
        {
            user = new User();
            user.setEmail(orderRequest.getEmail());
            user.setName(orderRequest.getName());
            user.setPhone(orderRequest.getPhone());
            user.setSurname(orderRequest.getSurname());
            user = userRepository.save(user);
        }

        Order order = new Order();
        order.setAddress(orderRequest.getAddress());
        order.setCreated(new Timestamp(System.currentTimeMillis()));
        order.setStatus(OrderStatus.ORDERED.getValue());
        order.setUser(user);
        order = orderRepository.save(order);

        List<OrderProduct> orderProducts = new ArrayList<OrderProduct>();
        for (ProductInfo prodInfo: orderRequest.getProducts())
        {
            Product product = products.stream().filter(x-> x.getId() == prodInfo.getId()).findFirst().get();
            OrderProduct orderProduct = new OrderProduct();
            orderProduct.setProduct(product);
            orderProduct.setOrder(order);
            orderProduct.setCount(prodInfo.getCount());
            orderProducts.add(orderProduct);
        }
        orderProductRepository.save(orderProducts);

        return ResponseEntity.ok(null);
    }
}
