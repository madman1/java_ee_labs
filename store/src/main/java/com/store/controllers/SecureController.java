package com.store.controllers;

import com.store.entities.Account;
import com.store.entities.User;
import com.store.repository.AccountRepository;
import com.store.repository.UserRepository;
import com.store.request.RegistrationRequest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import java.util.Date;

@CrossOrigin(origins = "*")
@Controller
public class SecureController
{
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/api/register", method = RequestMethod.POST)
    public ResponseEntity registerUser(@RequestBody RegistrationRequest request)
    {
        if (!request.getPhone().matches("[0-9]+") || request.getPhone().length() != 10 || request.getPassword().length() <= 2)
        {
            return ResponseEntity.status(400).body("Invalid credentials");
        }

        Account account = accountRepository.findByPhone(request.getPhone());
        if(account != null)
        {
            return ResponseEntity.status(400).body("Account with this phone number already exists");
        }

        account = new Account();
        account.setRole("user");
        account.setPassword(request.getPassword());
        account.setPhone(request.getPhone());
        Account res = accountRepository.save(account);

        User user = userRepository.findByPhone(request.getPhone());
        if(user == null)
        {
            user = new User();
        }

        user.setSurname(request.getSurname());
        user.setPhone(request.getPhone());
        user.setName(request.getName());
        user.setEmail(request.getEmail());
        userRepository.save(user);
        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/api/login", method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody Account login)
    {

        String jwtToken = "";

        if (login.getPhone() == null || login.getPassword() == null) {
            return ResponseEntity.status(400).body("Please fill in phone and password");
        }

        String phone = login.getPhone();
        String password = login.getPassword();

        Account user = accountRepository.findByPhone(phone);

        if (user == null) {
            return ResponseEntity.status(400).body("User phone not found");
        }

        String pwd = user.getPassword();

        if (!password.equals(pwd)) {
            return ResponseEntity.status(400).body("Invalid login. Please check your phone and password");

        }

        jwtToken = Jwts.builder().setSubject(phone).claim("role", user.getRole()).setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

        return ResponseEntity.ok(jwtToken);
    }
}
