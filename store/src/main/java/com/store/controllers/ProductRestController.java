package com.store.controllers;

import com.store.entities.Category;
import com.store.entities.Product;
import com.store.repository.CategoryRepository;
import com.store.repository.ProductRepository;
import com.store.request.ProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Random;

@CrossOrigin(origins = "*")
@Controller
public class ProductRestController
{
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping(value = "/api/product", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Product> getAll()
    {
        return productRepository.findAll();
    }

    @RequestMapping(value = "/api/product/active", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Product> getAllActive()
    {
        return productRepository.findAllByActive();
    }

    @RequestMapping(value = "/api/product/bycategory/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Product> getAllByCategory(@PathVariable int id)
    {
        return productRepository.findByCategoryId(id);
    }

    @RequestMapping(value = "/api/product/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Product getById(@PathVariable int id)
    {
        Product product = productRepository.findOne(id);

        if(product == null)
            return null;

        product.setViews(product.getViews() + 1);
        productRepository.save(product);
        return  product;
    }

    @RequestMapping(value = "/api/product/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<?> deleteById(@PathVariable int id)
    {
        productRepository.delete(id);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/api/product", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createNew(@RequestBody ProductRequest newProduct)
    {
        Random randomGenerator = new Random();
        Category category = categoryRepository.findOne(newProduct.getCategoryID());

        Product res = new Product();
        res.setDescription(newProduct.getDescription());
        res.setCreated(new Timestamp(System.currentTimeMillis()));
        res.setUpdated(new Timestamp(System.currentTimeMillis()));
        res.setName(newProduct.getName());
        res.setPrice(newProduct.getPrice());
        res.setActive(newProduct.getActive()? (byte)1 : (byte)0);
        res.setCode("BM " + randomGenerator.nextInt(10000));
        res.setViews((long)0);
        res.setCategory(category);
        productRepository.save(res);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/api/product", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<?> update(@RequestBody ProductRequest newProduct)
    {
        Category category = categoryRepository.findOne(newProduct.getCategoryID());

        Product res = new Product();
        res.setDescription(newProduct.getDescription());
        res.setUpdated(new Timestamp(System.currentTimeMillis()));
        res.setName(newProduct.getName());
        res.setPrice(newProduct.getPrice());
        res.setActive(newProduct.getActive()? (byte)1 : (byte)0);
        res.setCategory(category);
        productRepository.save(res);
        return ResponseEntity.ok(null);
    }
}
