package com.store.controllers;

import com.store.entities.User;
import com.store.repository.UserRepository;
import com.store.request.UserResponse;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

@CrossOrigin(origins = "*")
@Controller
public class UserRestController
{
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/api/secured/user", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(HttpServletRequest request)
    {
        Claims claims = (Claims) request.getAttribute("claims");
        String phoneNumber = claims.get("sub").toString();
        String role = claims.get("role").toString();

        if (phoneNumber == null || !phoneNumber.matches("[0-9]+") || phoneNumber.length() != 10)
        {
            return ResponseEntity.status(400).body("Invalid phone number");
        }

        User u = userRepository.findByPhone(phoneNumber);

        UserResponse result = new UserResponse(u, role);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(result);
    }
}
