package com.store;

import com.store.filters.JwtFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class DemoApplication {

	@Configuration
	static class WebConfig
	{
		@Bean
		public FilterRegistrationBean jwtFilter() {
			final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
			registrationBean.setFilter(new JwtFilter());
			registrationBean.addUrlPatterns("/api/secured/*");

			return registrationBean;
		}

		@Bean
		public WebMvcConfigurerAdapter forwardToIndex() {
			return new WebMvcConfigurerAdapter() {
				@Override
				public void addViewControllers(ViewControllerRegistry registry)
				{
					// forward ui related request to index.html index.html
					registry.addViewController("/faqs").setViewName(
							"forward:/index.html");

					registry.addViewController("/contact").setViewName(
							"forward:/index.html");

					registry.addViewController("/about").setViewName(
							"forward:/index.html");

					registry.addViewController("/checkout").setViewName(
							"forward:/index.html");

					registry.addViewController("/cart").setViewName(
							"forward:/index.html");

					registry.addViewController("/product").setViewName(
							"forward:/index.html");

					registry.addViewController("/success").setViewName(
							"forward:/index.html");

					registry.addViewController("/fail").setViewName(
							"forward:/index.html");


					registry.addViewController("/register").setViewName(
							"forward:/index.html");

					registry.addViewController("/login").setViewName(
							"forward:/index.html");

					registry.addViewController("/cabinet").setViewName(
							"forward:/index.html");
				}
			};
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
