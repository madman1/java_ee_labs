package com.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@EqualsAndHashCode
public class Product {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Name", nullable = true, length = 100)
    private String name;

    @Column(name = "Description", nullable = true, length = 1000)
    private String description;

    @Column(name = "Code", nullable = true, length = 45)
    private String code;

    @Column(name = "Views", nullable = true)
    private Long views;

    @Column(name = "Active", nullable = true)
    private Byte active;

    @Column(name = "Created", nullable = true)
    private Timestamp created;

    @Column(name = "Updated", nullable = true)
    private Timestamp updated;

    @Column(name = "Price", nullable = true, precision = 0)
    private Double price;

    @Column(name = "Image", nullable = true, length = 300)
    private String image;

    @Column(name = "ImageBig", nullable = true, length = 300)
    private String imageBig;

    @JsonIgnore
    @ManyToOne
    private Category category;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public void setViews(Long views) {
        this.views = views;
    }
    public void setActive(Byte active) {
        this.active = active;
    }
    public void setCreated(Timestamp created) {
        this.created = created;
    }
    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public void setCategory(Category category) {
        this.category = category;
    }
    public void setImage(String image) { this.image = image; }
    public void setImageBig(String imageBig) { this.imageBig = imageBig; }
}
