package com.store.entities;

import jdk.nashorn.internal.objects.annotations.*;
import lombok.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@EqualsAndHashCode
public class User {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Name", nullable = true, length = 150)
    private String name;

    @Column(name = "Surname", nullable = true, length = 150)
    private String surname;

    @Column(name = "Phone", nullable = true, length = 300)
    private String phone;

    @Column(name = "Email", nullable = true, length = 150)
    private String email;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setEmail(String email) {
        this.email = email;
    }
}
