package com.store.entities;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@EqualsAndHashCode
public class Category {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Name", nullable = true, length = 100)
    private String name;

    @Column(name = "Image", nullable = true, length = 45)
    private String image;

    @Column(name = "Active", nullable = true)
    private Byte active;

    @Column(name = "Created", nullable = true)
    private Timestamp created;

    @OneToMany(mappedBy="category")
    private List<Product> products;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public void setActive(Byte active) {
        this.active = active;
    }
    public void setCreated(Timestamp created) {
        this.created = created;
    }
    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
