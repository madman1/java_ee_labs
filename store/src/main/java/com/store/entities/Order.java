package com.store.entities;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.jws.soap.SOAPBinding;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "`order`")
public class Order {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Address", nullable = true, length = 300)
    private String address;

    @Column(name = "Created", nullable = true)
    private Timestamp created;

    @Column(name = "Status", nullable = true)
    private Integer status;

    /*@ManyToMany
    @JoinTable(name="order_product",
            joinColumns=@JoinColumn(name="Order_Id"),
            inverseJoinColumns=@JoinColumn(name="Product_Id"))
    private List<Product> products;*/

    @ManyToOne
    private User user;

    @OneToMany(mappedBy="order")
    private List<OrderProduct> orderProducts;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setCreated(Timestamp created) {
        this.created = created;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public void setProducts(List<OrderProduct> orderProducts) {
        this.orderProducts = orderProducts;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
