package com.store.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "`order_product`")
public class OrderProduct
{
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Count", nullable = false)
    private Integer count;

    @ManyToOne
    @PrimaryKeyJoinColumn(name="Order_Id", referencedColumnName="Id")
    private Order order;

    @ManyToOne
    @PrimaryKeyJoinColumn(name="Product_Id", referencedColumnName="Id")
    private Product product;
}
