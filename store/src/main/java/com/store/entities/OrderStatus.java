package com.store.entities;

public enum OrderStatus
{
    ORDERED(0), SENDED(1), FINISHED(2);
    private final int value;

    private OrderStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}