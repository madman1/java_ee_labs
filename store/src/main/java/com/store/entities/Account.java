package com.store.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@EqualsAndHashCode
public class Account
{
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Phone", nullable = false, length = 300)
    private String phone;

    @Column(name = "Password", nullable = false, length = 1000)
    private String password;

    @Column(name = "Role", nullable = false, length = 150)
    private String role;
}
