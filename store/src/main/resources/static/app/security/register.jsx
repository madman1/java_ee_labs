import React from 'react';
import ReactDOM from 'react-dom';
import { Link, browserHistory } from 'react-router-dom';
import AjaxHelper from './../services/ajax.js';

export default class Register extends React.Component {
   
   constructor(props)
   {
       super(props);
       this.clearValues.bind(this);
       this.ajaxHelper = new AjaxHelper();
   }
   componentWillMount()
   {
      this.clearValues();
   }

   nameChange(e)
   {
       let value = e.target.value;
       this.setState({name: value, nameValid: this.validateName(value)});
   }

   passwordChange(e)
   {
       let value = e.target.value;
       this.setState({password: value, passwordValid: this.validatePassword(value)});
   }

   surnameChange(e)
   {
       let value = e.target.value;
       this.setState({surname: value});
   }

   addressChange(e)
   {
       let value = e.target.value;
       this.setState({address: value});
   }

   emailChange(e)
   {
       let value = e.target.value;
       this.setState({email: value});
   }

   phoneChange(e)
   {
       let value = e.target.value;
       this.setState({phone: value, phoneValid: this.validatePhone(value)});
   }

   validateName(name)
   {
       return name.length > 2;
   }

   validatePhone(phone)
   {
       var m = phone.match(/\d/g);
       return m && m.length === 10
   }

   validatePassword(password)
   {
       return password.length > 3;
   }

   clearValues()
   {
        this.setState({
            name: "",
            surname: "",
            address: "",
            email: "",
            phone: "",
            password: "",
            phoneValid: true,
            nameValid: true,
            passwordValid: true,
            message: ""
        });
   }
   
   submitForm(e)
   {
       e.preventDefault();

       if(this.state.name == null ||
        this.state.phone == null ||
        this.state.password == null ||
        this.validateName(this.state.name) ===false || 
        this.validatePhone(this.state.phone)===false ||
        this.validatePassword(this.state.password) === false)
            return;
        


        let userInfo = {
            name: this.state.name,
            surname: this.state.surname,
            address: this.state.address,
            email: this.state.email,
            phone: this.state.phone,
            password: this.state.password
        };

        this.ajaxHelper.ajaxPostWithInfo('api/register', userInfo).then((result)=> {
            if(result.status >= 400)
            {
                let message = "Bad registration. This number is already used or registration parameters are incorrect";
                this.setState({message: message});
            }
            else{
                this.props.history.push('/login');
            }
        });

        
   }

    render()
    {

        let nameColor = this.state.nameValid===true?"#5d7c29":"red";
        let phoneColor = this.state.phoneValid===true?"#5d7c29":"red";
        let passwordColor = this.state.passwordValid === true? "#5d7c29":"red";

        return ( 
    <div id="content" >
    	<h2>Sign up</h2>
        
            <form onSubmit={this.submitForm.bind(this)}>
                <div className="form-divider">
                    <div className="content_half form_field">
                        Name*:
                        <input value={this.state.name} name="name" type="text" id="name" onChange={this.nameChange.bind(this)} style={{borderColor:nameColor}} maxLength="40" />
                        Surname:
                        <input value={this.state.surname} name="surname" type="text" id="surname" onChange={this.surnameChange.bind(this)} maxLength="40" />
                        Address:
                        <input value={this.state.address} name="address" type="text" id="address" onChange={this.addressChange.bind(this)} maxLength="40" />
                    </div>
                    
                    <div className="content_half form_field">
                        Phone*:<br />
                        <input value={this.state.phone} name="phone" type="text" id="phone" onChange={this.phoneChange.bind(this)} style={{borderColor:phoneColor}} maxLength="40" />
                        Password*:<br />
                        <input value={this.state.password} name="password" type="password" id="password" onChange={this.passwordChange.bind(this)} style={{borderColor:passwordColor}} maxLength="40" />
                        Email:
                        <input value={this.state.email} name="email" type="text" id="email" onChange={this.emailChange.bind(this)} maxLength="40" />
                    </div>
                </div>
                <div style={{marginTop: "15px"}}>
                    <input type="submit" className="button" value="Sign up" />
                </div>

                {this.state.message == "" ? <div></div>: 
                <div className="exceptionMessage">{this.state.message}</div>}

            </form> 
    </div>
       );
    }
}