import React from 'react';
import ReactDOM from 'react-dom';
import { Link, browserHistory } from 'react-router-dom';
import AjaxHelper from './../services/ajax.js';
import TokenStorage from './../services/tokenStorage.js';

export default class Login extends React.Component {
   
   constructor(props)
   {
       super(props);
       this.ajaxHelper = new AjaxHelper();
   }
   componentWillMount()
   {
       if(TokenStorage.tokenExist())
       {
           this.ajaxHelper.ajaxGet("api/secured/user", TokenStorage.getToken()).then(user=>{
                if(user != null)
                {
                    this.setState({
                       user: user
                    });
                }
            });

      this.ajaxHelper.ajaxGet("api/secured/order", TokenStorage.getToken()).then(orders=>{
                if(orders != null)
                {
                    this.setState({
                       orders: orders
                    });
                }
            });
       }
   }

    render()
    {
        let stateIsNotReady = this.state == null || this.state.user == null || this.state.orders == null;
        return ( 
    <div id="content" >
        <h2>Profile</h2>
        {
            stateIsNotReady ? 
            <div></div> :
            <div  className="order-description">
                <div>
                    <label>Name:  {this.state.user.name} </label>
                    <label>Surname:  {this.state.user.surname} </label>
                </div>
                <div>
                    <label>Email:  {this.state.user.email} </label>
                    <label>Phone:  {this.state.user.phone} </label>
                </div>
            </div>
        }
    	<h2>Orders</h2>
        {
        stateIsNotReady ?
        <div></div> :
        this.state.orders.map(order=> {
           return (
            <div key={order.id} className="order-description">
                <div>
                    <label>User name:  {order.name} </label>
                    <label>Surname:  {order.surname} </label>
                </div>
                <div>
                    <label>Address:  {order.address} </label>
                    <label>Email:  {order.email} </label>
                    <label>Phone:  {order.phone} </label>
                </div>
                <div>
                    <label>Order status:  {order.status} </label>
                    <label>Order date:  {order.date} </label>
                </div>
                <button>Details</button>{this.state.user.role == 'admin'  ? <button>Change status</button> : <span></span>}
            </div>)
        })}
            
    </div>
       );
    }
}