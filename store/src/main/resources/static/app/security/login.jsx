import React from 'react';
import ReactDOM from 'react-dom';
import { Link, browserHistory } from 'react-router-dom';
import AjaxHelper from './../services/ajax.js';
import TokenStorage from './../services/tokenStorage.js';

export default class Login extends React.Component {
   
   constructor(props)
   {
       super(props);
       this.clearValues.bind(this);
       this.ajaxHelper = new AjaxHelper();
   }
   componentWillMount()
   {
      this.clearValues();
   }

   passwordChange(e)
   {
       let value = e.target.value;
       this.setState({password: value, passwordValid: this.validatePassword(value)});
   }

   phoneChange(e)
   {
       let value = e.target.value;
       this.setState({phone: value, phoneValid: this.validatePhone(value)});
   }

   validatePhone(phone)
   {
       var m = phone.match(/\d/g);
       return m && m.length === 10
   }

   validatePassword(password)
   {
       return password.length > 3;
   }

   clearValues()
   {
        this.setState({
            phone: "",
            password: "",
            phoneValid: true,
            passwordValid: true,
            message: ""
        });
   }
   
   submitForm(e)
   {
       e.preventDefault();

       if(this.state.phone == null ||
        this.state.password == null ||
        this.validatePhone(this.state.phone)===false ||
        this.validatePassword(this.state.password) === false)
            return;

        let userInfo = {
            phone: this.state.phone,
            password: this.state.password
        };

        this.ajaxHelper.ajaxPostWithInfo('api/login', userInfo).then((result)=> {
            if(result.status >= 400)
            {
                result.text().then(message => {
                    this.setState({message: message});
                });
            }
            else{
                result.text().then(token=>{
                    TokenStorage.setToken(token);
                    this.props.history.push('/');
                }).catch(e => {console.log(e)});
            }
        });

        
   }

    render()
    {
        let phoneColor = this.state.phoneValid===true?"#5d7c29":"red";
        let passwordColor = this.state.passwordValid === true? "#5d7c29":"red";

        return ( 
    <div id="content" >
    	<h2>Login</h2>
        
            <form onSubmit={this.submitForm.bind(this)}>
                <div className="content_half form_field">
                    Phone*:<br />
                    <input value={this.state.phone} name="phone" type="text" id="phone" onChange={this.phoneChange.bind(this)} style={{borderColor:phoneColor}} maxLength="40" />
                    Password*:<br />
                    <input value={this.state.password} name="password" type="password" id="password" onChange={this.passwordChange.bind(this)} style={{borderColor:passwordColor}} maxLength="40" />
                </div>
            
                <div style={{marginTop: "15px"}}>
                    <input type="submit" className="button" value="Sign up" />
                </div>

                {this.state.message == "" ? <div></div>: 
                <div className="exceptionMessage">{this.state.message}</div>}

            </form> 
    </div>
       );
    }
}