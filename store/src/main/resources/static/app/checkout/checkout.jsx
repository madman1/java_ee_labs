import React from 'react';
import ReactDOM from 'react-dom';
import { Link, browserHistory } from 'react-router-dom';
import Cart from './../services/cart.js';
import AjaxHelper from './../services/ajax.js';
import TokenStorage from './../services/tokenStorage.js';

export default class Checkout extends React.Component {
   
   constructor(props)
   {
       super(props);
       this.clearValues.bind(this);
       this.ajaxHelper = new AjaxHelper();
   }
   componentWillMount()
   {
       this.clearValues();
       let total = Cart.getSumOfCart();
      
        this.setState({
            total: total
        });

       if(TokenStorage.tokenExist())
       {
            this.ajaxHelper.ajaxGet("api/secured/user", TokenStorage.getToken()).then(user=>{
                if(user != null)
                {
                    this.setState({
                        name: user.name,
                        surname: user.surname,
                        phone: user.phone,
                        email: user.email
                    });
                }
            })
       }
   }

   nameChange(e)
   {
       let value = e.target.value;
       this.setState({name: value, nameValid: this.validateName(value)});
   }

   surnameChange(e)
   {
       let value = e.target.value;
       this.setState({surname: value});
   }

   addressChange(e)
   {
       let value = e.target.value;
       this.setState({address: value});
   }

   emailChange(e)
   {
       let value = e.target.value;
       this.setState({email: value});
   }

   phoneChange(e)
   {
       let value = e.target.value;
       this.setState({phone: value, phoneValid: this.validatePhone(value)});
   }

   validateName(name)
   {
       return name.length > 2;
   }

   validatePhone(phone)
   {
       var m = phone.match(/\d/g);
       return m && m.length === 10
   }

   clearValues()
   {
        this.setState({
            name: "",
            surname: "",
            address: "",
            email: "",
            phone: "",
            phoneValid: true,
            nameValid: true
        });
   }
   
   submitForm(e)
   {
       e.preventDefault();

       if(this.state.name == null ||
        this.state.phone == null ||
        this.validateName(this.state.name) ===false || 
        this.validatePhone(this.state.phone)===false)
            return;
        
        let cart = Cart.getCart();

        if(cart == null)
            return;

        let orderInfo = {
            name: this.state.name,
            surname: this.state.surname,
            address: this.state.address,
            email: this.state.email,
            phone: this.state.phone,
            products: []
        };

        for(let key in cart)
        {
            let count = cart[key].count;
            orderInfo.products.push({id: Number(key), count: count});
        }

        this.ajaxHelper.ajaxPost('api/order', orderInfo).then((result)=> {
            if(result == true)
            {
                Cart.clearCart();
                this.props.history.push('/success');
            }
            else if(result == false){
                this.props.history.push('/fail');
            }
        });

        this.clearValues();
   }

    render()
    {

        var nameColor = this.state.nameValid===true?"#5d7c29":"red";
        var phoneColor = this.state.phoneValid===true?"#5d7c29":"red";

        return ( 
    <div id="content" >
    	<h2>Checkout</h2>
        
		<h3>BILLING DETAILS</h3>
            <form onSubmit={this.submitForm.bind(this)}>
                <div className="content_half left form_field">
                    Name*:
                    <input value={this.state.name} name="name" type="text" id="name" onChange={this.nameChange.bind(this)} style={{borderColor:nameColor}} maxLength="40" />
                    Surname:
                    <input value={this.state.surname} name="surname" type="text" id="surname" onChange={this.surnameChange.bind(this)} maxLength="40" />
                    Address:
                    <input value={this.state.address} name="address" type="text" id="address" onChange={this.addressChange.bind(this)} maxLength="40" />
                </div>
                
                <div className="content_half right form_field">
                    Email:
                    <input value={this.state.email} name="email" type="text" id="email" onChange={this.emailChange.bind(this)} maxLength="40" />
                    Phone*:<br />
                    <input value={this.state.phone} name="phone" type="text" id="phone" onChange={this.phoneChange.bind(this)} style={{borderColor:phoneColor}} maxLength="40" />
                    <span>Please, specify your reachable phone number to call you for a verification.</span>
                </div>
                
                <div className="cleaner h40"></div>
                
                <h3>SHOPPING CART</h3>

                {this.state.total != 0 ? 
                    <div>
                        <h4>TOTAL: <strong>${this.state.total}</strong></h4>
                        <input type="submit" className="button" value="Create order" style={{marginBottom: "15px"}}/>
                    </div> :
                    <h4>Cart is empty</h4>
                }
                
                <div className="blank_box">
                    <a href="#"><img src="images/free_shipping.jpg" alt="Free Shipping" /></a>
                </div> 
            </form> 
    </div>
       );
    }
}