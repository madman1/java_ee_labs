import React from 'react';
import ReactDOM from 'react-dom';
import AjaxHelper from './../services/ajax.js';
import Cart from './../services/cart.js';

export default class Product extends React.Component
{
    constructor(props)
    {
        super(props);
        this.productId = props.match.params.id;
        this.basicImagePath = "../../";
        this.ajaxHelper = new AjaxHelper();

        this.quantityChanged = this.quantityChanged.bind(this);
        this.addToCart = this.addToCart.bind(this);
    }

    componentWillMount()
    {
        this.ajaxHelper.ajaxGet("/api/product/" + this.productId).then((res)=> {
            if(res == null)
                return;
            
            this.setState(
            {
                quantity: 1,
                product: {
                    id: this.productId,
                    name: res.name,
                    price: res.price,
                    image: res.image,
                    imageBig: res.imageBig,
                    description: res.description,
                    active: res.active
                }
            });
        });
    }

    quantityChanged(e){
        this.setState({quantity: e.target.value});
    }

    addToCart() {
        Cart.addProduct(this.state.product, Number(this.state.quantity));
    }

    render(){
        return (
    <div id="content" >
        {this.state == null?
        <div></div> : 
        <div>
            <h2>{this.state.product.name}</h2>
            <div className="content_half left">
                <a  rel="lightbox" href={this.basicImagePath + this.state.product.imageBig}><img src={this.basicImagePath + this.state.product.image} alt={this.state.product.name} /></a>
            </div>
                <div className="content_half right">
                    <table>
                        <tbody>
                            <tr>
                                <td width="130">Price:</td>
                                <td width="84">${this.state.product.price}</td>
                            </tr>
                            <tr>
                                <td>Availability:</td>
                                <td><strong>{this.state.product.active ? "In Stock" : "Not Available"}</strong></td>
                            </tr>
                            <tr><td>Quantity</td><td><input type="number" value={this.state.quantity} min="1" step="1" maxLength="2" pattern="^[0-9]" onChange={this.quantityChanged} /></td></tr>
                        </tbody>
                    </table>
                    <div className="cleaner h20"></div>
                    <a href="#" className="button" onClick={this.addToCart}>Add to Cart</a>
                </div>
                <div className="cleaner h40"></div>
                
                <h4>{this.state.product.name + " Description"} </h4>
                <p>{this.state.product.description}</p>
                <div className="cleaner h40"></div>
            <div className="blank_box">
                <a href="#"><img src={this.basicImagePath + "images/free_shipping.jpg"} alt="Free Shipping" /></a>
            </div> 
        </div> 
        }  
    </div>
       );
    }
}