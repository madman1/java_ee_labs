import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

export default class Contact extends React.Component {

    textChange(){}

    render(){
        return ( 
    <div id="content" >
		<h2>Contact</h2>
        <p>Morbi adipiscing gravida lacus, id rhoncus neque sollicitudin ac. Sed eget purus vitae enim pulvinar viverra. Cras ut elit et ligula blandit eleifend. Nam at odio sem, sed tempor justo.</p>
        <div className="cleaner h20"></div>
        <div className="col col13">
            <h4>Mailing Address</h4>
        	<h6><strong>Office One</strong></h6>
            800-220 Fusce nec ante at odio, <br />
            In vitae lacus in purus, 66770<br />
            Diam a mollis tempor<br /><br />
            
			<strong>Phone:</strong> 010-440-5500<br />
            <strong>Email:</strong> <a href="mailto:contact@company.com">contact@company.com</a> <br />
            
            <div className="cleaner h20"></div>
            
            <h6><strong>Office Two</strong></h6>
            600-110 Duis lacinia, <br />
            Ullamcorper mattis, 88770<br />
            Maecenas a diam, mollis magna<br /><br />
            
			<strong>Phone:</strong> 020-660-8800<br />
            <strong>Email:</strong> <a href="mailto:info@company.com">info@company.com</a> <br />

		</div>
        <div className="col col23 no_margin_right">
        	<div className="map_border">
                <iframe width="430" height="340" frameBorder="0" scrolling="no" marginHeight="0" marginWidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Central+Park,+New+York,+NY,+USA&amp;aq=0&amp;sll=14.093957,1.318359&amp;sspn=69.699334,135.263672&amp;vpsrc=6&amp;ie=UTF8&amp;hq=Central+Park,+New+York,+NY,+USA&amp;ll=40.778265,-73.96988&amp;spn=0.033797,0.06403&amp;t=m&amp;output=embed"></iframe></div>
			
        </div>
        <div className="cleaner h40"></div>
        <div id="contact_form">
           <form method="post" name="contact" action="#">
           		<div className="col col13">
                
                    <label>Name:</label> 
              		<input name="name" type="text" className="input_field" id="name" maxLength="40" />
                	<div className="cleaner h10"></div>
                    <label >Email:</label> 
          			<input name="email" type="text" className="required input_field" id="email" maxLength="40" />
                	<div className="cleaner h10"></div>
                    <label >Phone:</label> 
        			<input name="phone" type="text" className="input_field" id="phone" maxLength="20" />
                	<div className="cleaner h10"></div>
                	
                
			 	</div>
                
                <div className="col col23 no_margin_right">
                    <label>Message:</label> 
               	  	<textarea id="message" name="message" rows="0" cols="0" className="required"></textarea>
                    <div className="cleaner h10"></div>
                    <input type="submit" className="submit_btn left" name="submit" id="submit" value="Send" />
                    <input type="reset" className="submit_btn submit_right" name="reset" id="reset" value="Reset" />
				</div>
                
                
            </form>
        </div>
        <div className="cleaner h40"></div>        
        <div className="blank_box">
        	<a href="#"><img src="images/free_shipping.jpg" alt="Free Shipping" /></a>
        </div>  
        <div className="cleaner"></div>  
    </div>
       );
    }
}