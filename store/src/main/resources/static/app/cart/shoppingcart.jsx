import React from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import Cart from './../services/cart.js'

export default class ShoppingCart extends React.Component {

    constructor(props)
    {
        super(props);

        this.basicImagePath = "../../";
        this.updateState = this.updateState.bind(this);
        this.setDefaultTextBoxState = this.setDefaultTextBoxState.bind(this);
    }

    componentWillMount()
    {
        this.updateState();
        this.setDefaultTextBoxState();
    }

    setDefaultTextBoxState()
    {
        let items = Cart.getCart();
        let tb = {};
        for(let productId in items)
        {
            let item = items[productId];
            tb[productId] = item.count;
        }
        this.setState({tb: tb});
    }

    tbValueChange(productId, e)
    {
        let temp = this.state.tb;
        temp[productId] = e.target.value;
        this.setState({tb: temp})
    }

    recalculatePrice(productId, e){
        
        console.log(e.target.value);
        let value = Number(e.target.value);
        if(!Number.isInteger(value))
            return;

        if(value == 0)
        {
            Cart.removeProduct(productId);
        }
        else
        {
            let countBeforeChange = this.state.items[productId].count;
            let countChange = value - countBeforeChange;
            console.log("Count change: " + countChange);
            Cart.addProduct({id: productId}, countChange);
        }

        this.updateState();
    }

    updateState()
    {
        let items = Cart.getCart();
        let total = Cart.getSumOfCart();
        this.setState({items: items, total: total});

        console.log(items);
    }

    removeProduct(productId)
    {
        Cart.removeProduct(productId);
        this.updateState();
    }

    render()
    {
        return (
        <div id="content">
            {this.state == null || this.state.items == null ?
            <h2>Cart is empty</h2> :
            <div>
                <h2>Shopping Cart</h2>
                <table className="cart-table">
                    <thead>
                        <tr style={{backgroundColor: "#395015"}} >
                            <th width="168" className="align-left">Item</th> 
                            <th width="188" className="align-left">Description</th> 
                            <th width="60" className="align-center">Quantity</th> 
                            <th width="80" className="align-right">Price</th> 
                            <th width="80" className="align-right">Total</th> 
                            <th width="64"> </th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            Object.keys(this.state.items).map((productId)=> {
                                let item = this.state.items[productId];
                                return (
                                <tr key={productId} style={{backgroundColor: "#41581B"}}>
                                    <td><img src={item.product.image} alt={item.product.name} /></td> 
                                    <td>{item.product.name}</td> 
                                    <td className="align-center"><input type="number" type="number" min="0" step="1" maxLength="2" value={this.state.tb[productId]}  onChange={this.tbValueChange.bind(this, productId)} onBlur={this.recalculatePrice.bind(this, productId)}/> </td>
                                    <td className="align-right">${item.product.price}</td> 
                                    <td className="align-right">${item.product.price * item.count}</td>
                                    <td className="align-center"> <a href="#" onClick={this.removeProduct.bind(this, productId)}><img src={this.basicImagePath + "images/remove.png"} alt="remove" /><br />Remove</a> </td>
                                </tr>
                            );

                        }, this)}
                        
                        <tr style={{backgroundColor: "#41581B"}}>
                            <td style={{columnSpan: 3}}></td>
                            <td className="align-right"><h4>All Total:</h4></td>
                            <td className="align-right"><h4>${this.state.total}</h4></td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
                <div className="cleaner h20"></div>
                <div className="right"><Link to="/checkout" className="button">checkout</Link></div>
                <div className="cleaner h20"></div>
                <div className="blank_box">
                    <a href="#"><img src={this.basicImagePath + "images/free_shipping.jpg" } alt="Free Shipping" /></a>
                </div> 
            </div> 
            }  
        </div>
        );
    }
}