import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, browserHistory, Link } from 'react-router-dom';

import Home from './home/home.jsx';
import Header from './main/header.jsx';
import Footer from './main/footer.jsx';
import Product from './productdetails/product.jsx';
import ShoppingCart from './cart/shoppingcart.jsx';
import About from './static/about.jsx';
import Contact from './static/contact.jsx';
import Faqs from './static/faqs.jsx';
import Checkout from './checkout/checkout.jsx';

import Register from './security/register.jsx';
import Login from './security/login.jsx';
import Cabinet from './security/cabinet.jsx';

class App extends React.Component {
    render() {
        return (
<Router history={browserHistory}>
        <div id="templatemo_wrapper_sp">
            <Header />
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    {/* main content here */}
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/product/:id" component={Product}/>
                        <Route path="/cart" component={ShoppingCart}/>
                        <Route path="/checkout" component={Checkout}/>
                        <Route path="/about" component={About}/>
                        <Route path="/contact" component={Contact}/>
                        <Route path="/faqs" component={Faqs}/>
                        <Route path="/success" component={SuccessOrder}/>
                        <Route path="/fail" component={FailedOrder}/>

                        <Route path="/register" component={Register}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/cabinet" component={Cabinet}/>

                        <Route path="*" component={NotFound}/>
                    </Switch>
                    <div className="cleaner"></div>
                </div>
            </div>
            <Footer/>
        </div>
</Router>
        );
    }
}
 
const NotFound = () => <h3>Not Found</h3>
const SuccessOrder = () => <h3>Your order successfully created</h3>
const FailedOrder = () => <h3>Exception appeared while creating your order</h3>

ReactDOM.render(<App/>, document.getElementById('container'));