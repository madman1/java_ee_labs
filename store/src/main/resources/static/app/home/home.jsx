import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import AjaxHelper from './../services/ajax.js';
import CategoryLink from './category.jsx';
import Cart from './../services/cart.js';

export default class Home extends React.Component {

    constructor(props)
    {
        super(props);
        this.ajaxHelper = new AjaxHelper();
        this.setProductsState = this.setProductsState.bind(this);
    }

    componentWillMount()
    {
        this.ajaxHelper.ajaxGet("api/product/active").then(this.setProductsState);
 
        this.ajaxHelper.ajaxGet("api/category").then((result)=> {
            if(result == null)
                return;
            
            let categories = result.map((item, index)=> {
                return {
                    id: item.id,
                    name: item.name
                };
            });

            this.setState({categories: categories});
        });
    }

    categoryClick(id)
    {
        if(id == 0)
        {
            this.ajaxHelper.ajaxGet("api/product/active").then(this.setProductsState);
        }
        else
        {
            this.ajaxHelper.ajaxGet("/api/product/bycategory/" + id).then(this.setProductsState);
        }
    }

    setProductsState(res)
    {
        if(res == null)
            return;

        let prods = res.map((item, index)=> {
                return {
                    id: item.id,
                    image: item.image, 
                    name: item.name,
                    price: item.price,
                    imageBig: item.imageBig,
                    description: item.description,
                    active: item.active
                };
            }); 

        this.setState({products: prods});
    }

    addToCart(product) {
        Cart.addProduct(product, 1);
    }

    render()
    {
        return (
    <div>
        <div id="sidebar" className="left">
            <div className="sidebar_box"><span className="bottom"></span>
                <h3>Categories</h3>   
                <div className="content"> 
                    <ul className="sidebar_list">
                        <CategoryLink key={0} categoryClick={this.categoryClick.bind(this, 0)} name="All" />
                        {
                            this.state == null || this.state.categories == null ?
                            <div></div> :
                            this.state.categories.map(function(item, index){
                                return (<CategoryLink key={item.id} categoryClick={this.categoryClick.bind(this, item.id)} name={item.name} />);
                            }, this)
                        }
                    </ul>
                </div>
            </div>
        </div>

        <div id="content" className="right">
		<h2>Welcome to Floral Shop</h2>
		<p>If you’re looking for a tried-and-true gift, you can’t go wrong when you send flowers. Our best seller bouquets have stood the test of time and are sure to delight, no matter the occasion.</p>
        
        { this.state == null || this.state.products == null ?
        <div></div> :
        this.state.products.map(function(item, index) {
            return (
            <div className="product_box" key={item.id}>
                <Link to="/product"><img src={item.image} alt={item.name} /></Link>
                <h3>{item.name}</h3>
                <p className="product_price">${item.price}</p>
                <p className="add_to_cart">
                    <Link to={"/product/" + item.id }>Detail</Link>
                    <a href="#" onClick={this.addToCart.bind(this, item)}>Add to Cart</a>
                </p>
            </div>);
        }, this)}
                 	
        </div>
    </div>);
    }
}