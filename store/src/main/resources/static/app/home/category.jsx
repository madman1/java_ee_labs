import React from 'react';
import ReactDOM from 'react-dom';

export default class CategoryLink extends React.Component{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
<li onClick={this.props.categoryClick}><a href="#">{this.props.name}</a></li>);
    }
}