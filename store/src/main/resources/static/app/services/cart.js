export default class Cart{

    static addProduct(newProd, count)
    {
        let prodId = newProd.id;
        let cartStorage = this.getCart();

        if(cartStorage == null)
        {
            cartStorage = {};
        }

        if(prodId in cartStorage)
        {
            console.log("Product exist")
            let existingValue = cartStorage[prodId];
            existingValue.count = existingValue.count + count;
            cartStorage[prodId] = existingValue;
        }
        else
        {
            cartStorage[prodId] = {
                count: count,
                product: newProd
            };
        }

        this._saveCart(cartStorage);
    }

    static removeProduct(prodId)
    {
        let jsonStr = sessionStorage.getItem("cart");
        let cartStorage = this.getCart();

        if(cartStorage == null)
            return;
        
        if(prodId in cartStorage)
        {
            delete cartStorage[prodId];
        }

        if(Object.keys(cartStorage).length == 0)
        {
            sessionStorage.removeItem("cart");
        }
        else
        {
            this._saveCart(cartStorage);
        }
    }

    static clearCart()
    {
        sessionStorage.removeItem("cart");
    }

    static getCart()
    {
        let jsonStr = sessionStorage.getItem("cart");
        let cartStorage = null;
        
        if(jsonStr)
        {
            cartStorage = JSON.parse(jsonStr);
        }

        return cartStorage;
    }

    static getSumOfCart()
    {
        let cartStorage = this.getCart()

        if(cartStorage == null)
            return 0;

        let sum = 0;
        for(let key in cartStorage)
        {
            if(cartStorage.hasOwnProperty(key))
            {
                let item = cartStorage[key];
                sum += item.count * item.product.price;
            }
        }

        return sum;
    }

    static _saveCart(cartStorage) 
    {
        let jsonStr = JSON.stringify(cartStorage);
        sessionStorage.setItem("cart", jsonStr);
    }
}