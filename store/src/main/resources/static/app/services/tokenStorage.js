
export default class TokenStorage
{
    static getToken()
    {
        return localStorage.getItem(this._getKey())
    }

    static setToken(token)
    {
        localStorage.setItem(this._getKey(), token);
    }

    static tokenExist()
    {
        return localStorage.getItem(this._getKey()) != null;
    }

    static clearToken()
    {
        localStorage.removeItem(this._getKey());
    }

    static _getKey()
    {
        return "token";
    }
}