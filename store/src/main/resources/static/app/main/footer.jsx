import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

export default class Footer extends React.Component {
    render(){
        return ( 
        <div id="templatemo_footer_wrapper">
            <div id="templatemo_footer">
                <div className="footer_left"></div>
                <div className="footer_right">
                    <p>
                        <Link to="/">Home</Link> | 
                        <Link to="/about">About</Link> | 
                        <Link to="/faqs">FAQs</Link> | 
                        <Link to="/checkout">Checkout</Link> | 
                        <Link to="/contact">Contact</Link>
                    </p>
                    <p>Copyright © 2017 <a href="#">Floral shop</a></p>
                </div>
                <div className="cleaner"></div>
            </div> 
        </div>       
       );
    }
}