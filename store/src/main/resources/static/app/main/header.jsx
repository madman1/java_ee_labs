import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import TokenStorage from './../services/tokenStorage.js';

export default class Header extends React.Component
{

 
    linkClick(e){
        var selectedElements = document.getElementsByClassName('selected');
        selectedElements[0].className = '';
        e.target.className = 'selected';
    }

    logoutClick()
    {
        TokenStorage.clearToken();
    }

    render(){

        let fullUrl = window.location.href;
        let parts = fullUrl.split('/');
        let url = parts[parts.length -1];

        return (        
        <div id="templatemo_header_wsp">
            <div id="templatemo_header" className="header_subpage">
                <div id="site_title"><a href="#">Floral Shop</a></div>
                <div id="templatemo_menu" className="ddsmoothmenu">
                    <ul>
                        <li><Link to="/" className={url == "" || url == "/" || url == "#" ? "selected" : ""} onClick={this.linkClick}>Home</Link></li>
                        <li><Link to="/cart" className={url == "cart" ? "selected" : ""} onClick={this.linkClick}>Cart</Link></li>
                        <li><Link to="/checkout" className={url == "checkout" ? "selected" : ""} onClick={this.linkClick}>Checkout</Link></li>
                        <li><Link to="/about" className={url == "about" ? "selected" : ""} onClick={this.linkClick}>About</Link></li>
                        <li><Link to="/contact" className={url == "contact" ? "selected" : ""} onClick={this.linkClick}>Contact</Link></li>
                        <li><Link to="/faqs" className={url == "faqs" ? "selected" : ""}  onClick={this.linkClick}>FAQs</Link></li>
                    </ul>
                    <div className="login-menu ddsmoothmenu">
                        {TokenStorage.tokenExist() ? 
                        <ul>
                            <li><Link to="/cabinet" className={url == "cabinet" ? "selected" : ""} onClick={this.linkClick}>Cabinet</Link></li>
                            <li><Link to="/" onClick={this.logoutClick.bind(this)}>Logout</Link></li>
                        </ul>:

                        <ul>
                            <li><Link to="/register" className={url == "register" ? "selected" : ""} onClick={this.linkClick}>Sign up</Link></li>
                            <li><Link to="/login" className={url == "login" ? "selected" : ""} onClick={this.linkClick}>Login</Link></li>
                        </ul>}
                    </div>
                    <br style={{clear: 'left'}} />
                </div> 
            </div> 
        </div> );
    }
}