package com.store;

import com.store.entities.Category;
import com.store.entities.Order;
import com.store.entities.Product;
import com.store.entities.User;
import com.store.repository.CategoryRepository;
import com.store.repository.OrderRepository;
import com.store.repository.ProductRepository;
import com.store.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Test
	public void contextLoads() {


		List<Product> products  = productRepository.findAllByActive();
		List<Product> sfg  = productRepository.findByCategoryId(1);

		//UserDao d = new UserDao();
		/*User u = new User();
		u.setName("Ivan");
		u.setSurname("Petrov");

		List<User> users = userRepository.findAll();*/
		//userRepository.saveAndFlush(u);
		//d.save(u);*/

		assertTrue(true);
	}

	@Test
	public void ProductTest()
	{
		Category category = new Category();
		category.setName("Roses");
		category.setActive((byte)1);
		category.setCreated(new Timestamp(System.currentTimeMillis()));
		categoryRepository.save(category);
		//Category category = categoryRepository.findOne(1);

		/*Product p = new Product();
		p.setName("Water");
		p.setPrice(10.5);
		p.setDescription("");
		p.setCategory(category);
		p.setUpdated(new Timestamp(System.currentTimeMillis()));
		p.setCreated(new Timestamp(System.currentTimeMillis()));
		productRepository.saveAndFlush(p);
		List<Product> products = productRepository.findAll();*/
	}

	@Test
	public void OrderTest()
	{
		/*User u = new User();
		u.setName("Ivan");
		u.setSurname("Petrov");
		u.setEmail("tar@gmail.com");
		userRepository.saveAndFlush(u);*/

		User u = userRepository.findOne(1);

		Product p = productRepository.findOne(1);

		Order o = new Order();
		o.setCreated(new Timestamp(System.currentTimeMillis()));
		o.setAddress("Ivanovicha 10");

		o.setStatus(1);

		ArrayList<Product> products = new ArrayList<Product>();
		products.add(p);

		//o.setProducts(products);
		o.setUser(u);
		orderRepository.saveAndFlush(o);
	}
}
