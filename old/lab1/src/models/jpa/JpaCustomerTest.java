package models.jpa;

import models.DAOFactory;
import models.daos.CustomerDaoJdbc;
import models.daos.CustomerDaoJpa;
import models.entities.Market;
import models.utilities.DbAccessException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by taras_000 on 11-Apr-17.
 */
class JpaCustomerTest {

    CustomerDaoJpa _customerDao;

    @org.junit.jupiter.api.BeforeEach
    void setUp()
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        _customerDao =  context.getBean("jpaDao", CustomerDaoJpa.class);


        //_customerDao = new CustomerDaoJpa();
    }

    @Test
    void testJpa() throws DbAccessException {

        //_customerDao.delete("e907f2fc-93a5-40ce-83d0-f4bb3c8ed556");

        CustomerDaoJpa dao = new CustomerDaoJpa();
        List<Customer> customers = dao.getAll();

       /* EntityManagerFactory factory  = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        EntityManager entityManager = factory.createEntityManager();

        Customer c = entityManager.find(models.jpa.Customer.class,"5a8956bd-e2b7-48ed-827f-9dc2b628fa37");

        entityManager.getTransaction().begin();

        c.setName("Vitaliy");
        Customer c1 = entityManager.merge(c);

        Customer newCustomer = new Customer();
        newCustomer.setId(UUID.randomUUID().toString());
        newCustomer.setName("Marta");
        newCustomer.setAddress("USA, Arizona");
        entityManager.persist(newCustomer);

        entityManager.getTransaction().commit();

        String query = "select c from Customer c";
        List<Customer> customers = entityManager.createQuery(query, models.jpa.Customer.class).getResultList();*/
    }

    @Test
    void getAll()
    {
        Customer c1 = new Customer();
        c1.setId(UUID.randomUUID().toString());
        c1.setAddress("Yangelya 20");
        c1.setName("Taras");

        Customer c2 = new Customer();
        c2.setId(UUID.randomUUID().toString());
        c2.setAddress("Borshagovka 21");
        c2.setName("Ivan");

        try {
            // test of save
            _customerDao.save(c1);
            _customerDao.save(c2);

            // test of get all
            List<Customer> customers = _customerDao.getAll();

            Assertions.assertTrue(customers.size() >= 2);
            assertTrue(customers.stream().anyMatch(x-> x.getId().equals(c2.getId())));
            assertTrue(customers.stream().anyMatch(x-> x.getId().equals(c1.getId())));

            //test of get by id
            Customer customer = _customerDao.getEntityById(c1.getId());
            assertTrue(customer != null);
            assertTrue(customer.getName().equals("Taras"));
            assertTrue(customer.getAddress().equals("Yangelya 20"));

            // test delete
            _customerDao.delete(c1.getId());
            _customerDao.delete(c2.getId());

            customers = _customerDao.getAll();
            assertFalse(customers.stream().anyMatch(x-> x.getId().equals(c2.getId())));
            assertFalse(customers.stream().anyMatch(x-> x.getId().equals(c1.getId())));
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void saveWithException()
    {
        Customer newCustomer = new Customer();
        newCustomer.setName("TarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTaras");
        newCustomer.setAddress("Kyiv, Yangelya 20");

        try {
            _customerDao.save(newCustomer);
            fail("Exception not fallen");
        }
        catch (DbAccessException ex)
        {
            assertTrue(true);
        }
    }
}