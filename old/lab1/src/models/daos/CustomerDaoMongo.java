package models.daos;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import models.IDao;
import models.entities.Customer;
import models.utilities.DbAccessException;
import org.bson.*;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CustomerDaoMongo extends BaseDaoMongo implements IDao<Customer>
{
    public CustomerDaoMongo(String host, int port, String db, String collection)
    {
        super(host, port, db, collection);
    }

    @Override
    public List<Customer> getAll() throws DbAccessException
    {
        try {
            MongoCollection<Document> collection = getCollection();
            FindIterable<Document> docs = collection.find();

            List<Customer> customers = new ArrayList<Customer>();
            for (Document doc : docs) {
                customers.add(DocumentToObject(doc));
            }

            return customers;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while retreiving objects from db");
        }
    }

    @Override
    public Customer getEntityById(UUID id) throws DbAccessException
    {
        try {
            MongoCollection<Document> collection = getCollection();

            BsonDocument searchQuery = new BsonDocument();
            searchQuery.put("Id", new BsonString(id.toString()));

            FindIterable<Document> docs = collection.find(searchQuery);
            Document doc = docs.first();

            if(doc == null)
            {
                return null;
            }

            return DocumentToObject(doc);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while retreiving objects from db");
        }
    }

    @Override
    public void delete(UUID id) throws DbAccessException
    {
        try {
            MongoCollection<Document> collection = getCollection();

            BsonDocument searchQuery = new BsonDocument();
            searchQuery.put("Id", new BsonString(id.toString()));

            collection.deleteOne(searchQuery);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while retreiving objects from db");
        }
    }

    @Override
    public void save(Customer entity) throws DbAccessException
    {
        try {
            MongoCollection<Document> collection = getCollection();
            Document doc = ObjectToDocument(entity);
            collection.insertOne(doc);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while retreiving objects from db");
        }
    }

    private Customer DocumentToObject(Document document)
    {
        Customer c = new Customer();
        c.set_id(UUID.fromString(document.get("Id").toString()));
        c.set_name(document.get("Name").toString());
        c.set_address(document.get("Address").toString());
        return c;
    }

    private Document ObjectToDocument(Customer customer)
    {
        Document doc = new Document();
        doc.put("Id", customer.get_id().toString());
        doc.put("Name", customer.get_name());
        doc.put("Address", customer.get_address());
        return doc;
    }
}
