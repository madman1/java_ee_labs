package models.daos;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class BaseDaoMongo
{
    private String _host;
    private int _port;
    private String _db;
    private String _collection;

    public BaseDaoMongo(String host, int port, String db, String collection)
    {
        _host = host;
        _port = port;
        _db = db;
        _collection = collection;
    }

    protected MongoCollection<Document> getCollection()
    {
        MongoClient mongo = new MongoClient( _host , _port);
        MongoDatabase db = mongo.getDatabase(_db);
        MongoCollection<org.bson.Document> collection = db.getCollection(_collection);
        return collection;
    }
}
