package models.daos;

import models.IDao;
import models.utilities.DbAccessException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import models.jpa.Customer;

public class CustomerDaoJpa extends BaseJpaDao<Customer>
{
    @Override
    protected String getEntityName() {
        return "Customer";
    }

    @Override
    protected Class getEntityClass()
    {
        return Customer.class;
    }
}
