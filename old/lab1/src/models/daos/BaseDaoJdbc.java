package models.daos;
import javax.sql.DataSource;
import java.sql.*;

public abstract class BaseDaoJdbc
{
    private String _dbUrl;
    private String _user;
    private String _password;
    private DataSource _dataSource;

    public BaseDaoJdbc(String dbUrl, String user, String password)
    {
        _dbUrl = dbUrl;
        _user = user;
        _password = password;
    }

    public BaseDaoJdbc(DataSource dataSource)
    {
        _dataSource = dataSource;
    }

    protected Connection getConnection() throws SQLException
    {
        if(_dataSource !=null)
        {
            return _dataSource.getConnection();
        }
        return DriverManager.getConnection(_dbUrl, _user, _password);
    }

    protected void closeQuietly(Connection connection)
    {
        try { connection.close(); } catch (Exception e) { /* ignored */ }
    }

    protected void closeQuietly(PreparedStatement st)
    {
        try { st.close(); } catch (Exception e) { /* ignored */ }
    }

    protected void closeQuietly(ResultSet resultSet)
    {
        try { resultSet.close(); } catch (Exception e) { /* ignored */ }
    }
}
