package models.daos;

import models.jpa.Customer;
import models.utilities.DbAccessException;

import javax.ejb.EJB;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@EJB
public abstract class BaseJpaDao<T>
{
    protected Class entityClass;
    protected String entityName;

    //@PersistenceContext(unitName = "NewPersistenceUnit")
    protected EntityManager entityManager;

    public BaseJpaDao()
    {
        entityClass = getEntityClass();
        entityName = getEntityName();
        EntityManagerFactory factory  = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        entityManager = factory.createEntityManager();
    }

    protected abstract String getEntityName();
    protected abstract Class getEntityClass();

    public List<T> getAll() throws DbAccessException
    {
        try {
            String query = "select c from " + entityName +" c";
            List<T> customers = entityManager.createQuery(query, entityClass).getResultList();
            return customers;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while retreiving objects from db");
        }
    }

     public T getEntityById(String id) throws DbAccessException
     {
         try
         {
             entityManager.getTransaction().begin();
             T obj = (T)entityManager.find(entityClass,id);
             entityManager.getTransaction().commit();
             return (T)obj;
         }
         catch (Exception ex)
         {
             entityManager.getTransaction().rollback();
             ex.printStackTrace();
             throw new DbAccessException("Exception appeared while retreiving object from db. Id=" + id);
         }
     }

     public void delete(String id) throws DbAccessException
     {
         try
         {
             T obj = getEntityById(id);
             entityManager.getTransaction().begin();
             entityManager.remove(obj);
             entityManager.getTransaction().commit();
         }
         catch (Exception ex)
         {
             entityManager.getTransaction().rollback();
             ex.printStackTrace();
             throw new DbAccessException("Exception appeared while deleting of object from db. Id=" + id);
         }
     }

     public void save(T entity) throws DbAccessException
     {
         try
         {
             entityManager.getTransaction().begin();
             entityManager.merge(entity);
             entityManager.getTransaction().commit();
         }
         catch (Exception ex)
         {
             entityManager.getTransaction().rollback();
             ex.printStackTrace();
             throw new DbAccessException("Exception appeared while inserting of object into db" + entity);
         }
     }
}
