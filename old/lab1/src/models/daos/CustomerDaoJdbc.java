package models.daos;

import models.IDao;
import models.entities.Customer;
import models.utilities.DbAccessException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CustomerDaoJdbc extends BaseDaoJdbc implements IDao<Customer>
{
    public CustomerDaoJdbc(String dbUrl, String user, String password)
    {
        super(dbUrl, user, password);
    }

    public CustomerDaoJdbc(DataSource dataSource)
    {
       super(dataSource);
    }

    @Override
    public List<Customer> getAll() throws DbAccessException {
        List<Customer> list = new ArrayList<Customer>();
        Connection conn = null;
        PreparedStatement statement = null;

        try {
            conn = getConnection();
            String sql = "SELECT id, name, address FROM customer ORDER BY name";
            statement = conn.prepareStatement(sql);

            ResultSet result = statement.executeQuery();

            while (result.next())
            {
                if(result.getString(1) == null)
                {
                    continue;
                }

                Customer obj = new Customer();
                obj.set_id(UUID.fromString(result.getString(1)));
                obj.set_name(result.getString(2));
                obj.set_address(result.getString(3));
                list.add(obj);
            }

            return list;
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while retreiving objects from db");
        }
        finally {
            closeQuietly(conn);
            closeQuietly(statement);
        }
    }

    @Override
    public Customer getEntityById(UUID id) throws DbAccessException
    {
        Connection conn = null;
        PreparedStatement statement = null;

        try {
            conn = getConnection();
            String sql = "SELECT id, name, address FROM customer WHERE Id=?";
            statement = conn.prepareStatement(sql);
            statement.setString(1, id.toString());

            ResultSet result = statement.executeQuery();

            if (result.next())
            {
                Customer obj = new Customer();
                obj.set_id(UUID.fromString(result.getString(1)));
                obj.set_name(result.getString(2));
                obj.set_address(result.getString(3));
                return obj;
            }

            return null;
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while retreiving object from db. Id=" + id);
        }
        finally {
            closeQuietly(conn);
            closeQuietly(statement);
        }
    }

    @Override
    public void delete(UUID id) throws DbAccessException
    {
        Connection conn = null;
        PreparedStatement statement = null;

        try
        {
            conn = getConnection();

            String sql = "DELETE FROM customer WHERE Id=?";
            statement = conn.prepareStatement(sql);
            statement.setString(1, id.toString());

            statement.execute();
            conn.close();
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while deleting of object from db. Id=" + id);
        }
        finally {
            closeQuietly(conn);
            closeQuietly(statement);
        }
    }

    @Override
    public void save(Customer entity) throws DbAccessException
    {
        Connection conn = null;
        PreparedStatement statement = null;
        String sql = null;

        if(entity.get_id() == null)
        {
            throw new DbAccessException("Id of object is null" + entity);
        }

        Customer existing = getEntityById(entity.get_id());

        if(existing == null)
        {
            sql = "INSERT INTO customer (name, address, id) values (?, ?, ?)";
        }
        else
        {
            sql = "update customer set Name=?, Address =? where Id=?";
        }

        try
        {
            conn = getConnection();

            statement = conn.prepareStatement(sql);
            statement.setString(1, entity.get_name());
            statement.setString(2, entity.get_address());
            statement.setString(3, entity.get_id().toString());

            statement.executeUpdate();
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while inserting of object into db" + entity);
        }
        finally {
            closeQuietly(conn);
            closeQuietly(statement);
        }
    }
}
