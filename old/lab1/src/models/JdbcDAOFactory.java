package models;

import models.daos.CustomerDaoJdbc;
import models.entities.Customer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class JdbcDAOFactory extends DAOFactory
{
    private String _dbUrl;
    private String _user;
    private String _password;
    private DataSource _dataSource;

    public JdbcDAOFactory(String dbUrl, String user, String password)
    {
        _dbUrl = dbUrl;
        _user = user;
        _password = password;
    }

    public JdbcDAOFactory(DataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public JdbcDAOFactory()
    {
        _dataSource = initDataSource();
    }

    @Override
    public IDao<Customer> getCustomerDAO() {
        if(_dataSource != null)
        {
            return new CustomerDaoJdbc(_dataSource);
        }
        return new CustomerDaoJdbc(_dbUrl, _user, _password);
    }

    private static DataSource initDataSource()
    {
        // JNDI true way of getting connection (allows to use tomcat connection pool)
        Context x = null;
        try
        {
            x = new InitialContext();

            Context envContext = (Context) x.lookup("java:/comp/env");
            return (DataSource) envContext.lookup("jdbc/storeDb");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
