package models;

import models.entities.Customer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.sql.DataSource;

public abstract class DAOFactory
{
    // List of DAO types supported by the factory
    public static final int JDBC = 1;
    public static final int MONGO = 2;
    public static final int JDBC_TEST = 3;

    public abstract IDao<Customer> getCustomerDAO();

    public static DAOFactory getDAOFactory(int whichFactory) {
        switch (whichFactory) {
            case JDBC:

                DataSource dataSource = initDataSource();
                return new JdbcDAOFactory(dataSource);

            case JDBC_TEST:

                String url = "jdbc:mysql://localhost:3306/store";
                String user = "root";
                String password = "1111";
                return new JdbcDAOFactory(url, user, password);

            case MONGO:

                String host = "localhost";
                int port = 27017;
                String db = "store";

                return new MongoDAOFactory(host, port, db);
            default:
                return null;
        }
    }

    private static DataSource initDataSource()
    {
        // JNDI true way of getting connection (allows to use tomcat connection pool)
        Context x = null;
        try
        {
            x = new InitialContext();

            Context envContext = (Context) x.lookup("java:/comp/env");
            return (DataSource) envContext.lookup("jdbc/storeDb");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
