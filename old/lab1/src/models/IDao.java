package models;

import models.utilities.DbAccessException;

import java.util.List;
import java.util.UUID;

public interface IDao<E>
{
    public abstract List<E> getAll() throws DbAccessException;
    public abstract E getEntityById(UUID id) throws DbAccessException;
    public abstract void delete(UUID id) throws DbAccessException;
    public abstract void save(E entity) throws DbAccessException;
}
