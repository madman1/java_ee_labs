package models;


import models.daos.CustomerDaoMongo;
import models.entities.Customer;

public class MongoDAOFactory extends DAOFactory
{
    private String _host;
    private int _port;
    private String _db;

    public MongoDAOFactory(String host, int port, String db)
    {
        _host = host;
        _port = port;
        _db = db;
    }

    @Override
    public IDao<Customer> getCustomerDAO() {
        return new CustomerDaoMongo(_host , _port, _db, "customer");
    }
}
