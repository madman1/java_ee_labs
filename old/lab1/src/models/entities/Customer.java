package models.entities;

import com.sun.istack.internal.NotNull;

import java.util.UUID;

public class Customer
{
    public Customer()
    {
        _id = UUID.randomUUID();
    }
    private UUID _id;
    @NotNull
    private String _name;
    @NotNull
    private String _address;

    public String get_address() {
        return _address;
    }

    public void set_address(String _address) {
        this._address = _address;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public UUID get_id() {
        return _id;
    }

    public void set_id(UUID _id) {
        this._id = _id;
    }
}
