package models.entities;

/**
 * Created by taras_000 on 01-Apr-17.
 */
public class Market {
    private Customer _customer;

    protected  String Temp;

    public Market(Customer c)
    {
        _customer = c;
    }

    public Customer get_customer() {
        return _customer;
    }

    public void set_customer(Customer _customer) {
        this._customer = _customer;
    }
}
