package tests;

import models.DAOFactory;
import models.IDao;
import models.entities.Customer;
import models.entities.Market;
import models.utilities.DbAccessException;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.junit.jupiter.api.Assertions;
    import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.spi.PersistenceProvider;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

    import static org.junit.jupiter.api.Assertions.*;

class CustomerDaoTest
{
    IDao<Customer> _customerDao;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
       // DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.JDBC_TEST);
        //_customerDao = daoFactory.getCustomerDAO();

        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        DAOFactory daoFactory =  context.getBean("daoFactoryTest", DAOFactory.class);
        _customerDao = daoFactory.getCustomerDAO();
    }

    @Test
    void testInjection()
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        Customer obj = (Customer) context.getBean("customer");

        assertTrue(obj.get_name().equals("Ivan Ivanov"));
        Market m = (Market) context.getBean("market");
        assertTrue(m.get_customer().get_name().equals("Ivan Ivanov"));
    }

    @Test
    void getAllJdbs()
    {
        getAll();
    }

    @Test
    void getAllMongo()
    {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MONGO);
        _customerDao = daoFactory.getCustomerDAO();
        getAll();
    }

    @Test
    void getAll()
    {
        Customer c1 = new Customer();
        c1.set_id(UUID.randomUUID());
        c1.set_address("Yangelya 20");
        c1.set_name("Taras");

        Customer c2 = new Customer();
        c2.set_id(UUID.randomUUID());
        c2.set_address("Borshagovka 21");
        c2.set_name("Ivan");

        try {
            // test of save
            _customerDao.save(c1);
            _customerDao.save(c2);

            // test of get all
            List<Customer> customers = _customerDao.getAll();

            Assertions.assertTrue(customers.size() >= 2);
            assertTrue(customers.stream().anyMatch(x-> x.get_id().equals(c2.get_id())));
            assertTrue(customers.stream().anyMatch(x-> x.get_id().equals(c1.get_id())));

            //test of get by id
            Customer customer = _customerDao.getEntityById(c1.get_id());
            assertTrue(customer != null);
            assertTrue(customer.get_name().equals("Taras"));
            assertTrue(customer.get_address().equals("Yangelya 20"));

            // test delete
            _customerDao.delete(c1.get_id());
            _customerDao.delete(c2.get_id());

            customers = _customerDao.getAll();
            assertFalse(customers.stream().anyMatch(x-> x.get_id().equals(c2.get_id())));
            assertFalse(customers.stream().anyMatch(x-> x.get_id().equals(c1.get_id())));
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void saveWithException()
    {
        Customer newCustomer = new Customer();
        newCustomer.set_name("TarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTarasTaras");
        newCustomer.set_address("Kyiv, Yangelya 20");

        try {
            _customerDao.save(newCustomer);
            fail("Exception not fallen");
        }
        catch (DbAccessException ex)
        {
            assertTrue(true);
        }
    }

    @Test
    void getEntityById()
    {
        try {
           // Customer customer = _customerDao.getEntityById(UUID.fromString("8d9af85a-d518-4f73-98a4-8dcaa8303410"));
        }
        catch (Exception ex)
        {}
    }
}