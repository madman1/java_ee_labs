package controllers;

import models.DAOFactory;
import models.IDao;
import models.entities.Customer;
import models.utilities.DbAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomersServlet extends HttpServlet
{
    private IDao<Customer> _customerDao;
    /*public CustomersServlet(Customer customer)
    {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.JDBC);
        _customerDao = daoFactory.getCustomerDAO();
    }*/

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        DAOFactory factory = applicationContext.getBean("daoFactory", DAOFactory.class);

        _customerDao = factory.getCustomerDAO();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String errorMessage = request.getParameter("error");
        List<Customer> customers = null;
        try
        {
            customers = _customerDao.getAll();
        }
        catch (DbAccessException e)
        {
            e.printStackTrace();
            errorMessage = e.getMessage();
        }

        if(errorMessage != null && !errorMessage.isEmpty())
        {
            request.setAttribute("error", errorMessage);
        }

        request.setAttribute("customers", customers);
        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/Customers.jsp");

        if (dispatcher != null){
            dispatcher.forward(request, response);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String userId = request.getParameter("id");

        if(userId != null && !userId.isEmpty())
        {
            delete(request, response);
            return;
        }

        String username = request.getParameter("username");
        String address = request.getParameter("address");

        if(username == null || address == null || username.isEmpty() || address.isEmpty())
        {
            response.sendRedirect("/customers?error=Name and address should not be empty");
            return;
        }

        Customer c1 = new Customer();
        c1.set_name(username);
        c1.set_address(address);

        try
        {
            _customerDao.save(c1);
        }
        catch (DbAccessException e)
        {
            e.printStackTrace();
            response.sendRedirect("/customers?error="+e.getMessage());
            return;
        }

        response.sendRedirect("/customers");
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            _customerDao.delete(UUID.fromString(request.getParameter("id")));
        } catch (DbAccessException e) {
            e.printStackTrace();
            response.sendRedirect("/customers?error="+e.getMessage());
            return;
        }

        response.sendRedirect("/customers");
    }
}
