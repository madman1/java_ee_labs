package controllers;

import com.google.gson.Gson;
import com.mongodb.util.JSON;
import models.DAOFactory;
import models.daos.CustomerDaoJdbc;
import models.daos.CustomerDaoJpa;
import models.jpa.Customer;
import models.utilities.DbAccessException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class CustomerRestServlet extends HttpServlet
{
    private CustomerDaoJpa _customerDao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        _customerDao = applicationContext.getBean("jpaDao", CustomerDaoJpa.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            String idParam = request.getParameter("id");

            response.setContentType("application/json");
            Gson gson = new Gson();
            String result;

            if(idParam != null && !idParam.isEmpty())
            {
                Customer customer = _customerDao.getEntityById(idParam);
                result = gson.toJson(customer);
            }
            else
            {
                List<Customer> customers = _customerDao.getAll();
                result = gson.toJson(customers);
            }

            PrintWriter out = response.getWriter();
            out.print(result);
            out.flush();
        }
        catch (DbAccessException ex)
        {
            response.setStatus(400);
            response.resetBuffer();
        }
        catch (Exception ex)
        {
            response.setStatus(500);
            response.resetBuffer();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            String idParam = request.getParameter("id");

            if(idParam != null && !idParam.isEmpty())
            {
                _customerDao.delete(idParam);
                response.setStatus(200);
            }
            else
            {
                response.setStatus(404);
                return;
            }
        }
        catch (DbAccessException ex)
        {
            response.setStatus(400);
            response.resetBuffer();
        }
        catch (Exception ex)
        {
            response.setStatus(500);
            response.resetBuffer();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        save(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        save(request, response);
    }

    private void save(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);

            Gson gson = new Gson();
            Customer c = gson.fromJson(jb.toString(), Customer.class);

            _customerDao.save(c);
        }
        catch (DbAccessException ex)
        {
            response.setStatus(400);
            response.resetBuffer();
        }
        catch (Exception ex)
        {
            response.setStatus(500);
            response.resetBuffer();
        }
    }
}
