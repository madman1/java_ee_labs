<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>


<html>
<link>
    <title>Title</title>
    <link rel="stylesheet" href="Styles.css"/>
</head>
<body>
<div class="wrapper">
    <h2>Add new customer</h2>
    <form class="form-main" action="customers" method="post">
        <div class="form-group">
            <label>Name</label>
            <input class="form-control" type="text" name="username">
        </div>
        <div class="form-group">
            <label>Address</label>
            <input class="form-control" type="text" name="address">
        </div>
        <input type="submit" value="submit">
    </form>
    <div class="error">
        <c:if test="${not empty error}"><span class="error-text">${error}</span></c:if>
    </div>
</div>
<div class="wrapper">
    <h2>List of customers</h2>
    <table>
        <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Address</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${customers}" var="c">
                <tr class="table-row">
                    <td>
                        <form action="customers" method="post">
                            <input type="hidden" name="id" value="${c._id}">
                            <input class="btn-remove" type="submit" value="remove">
                        </form>
                    </td>
                    <td>${c._name}</td>
                    <td>${c._address}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
