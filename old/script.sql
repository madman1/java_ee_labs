

-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -------------------------------------shopshopshopshopshopshop----------------
CREATE TABLE IF NOT EXISTS `shop`.`User` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(150) NULL,
  `Surname` VARCHAR(150) NULL,
  `Phone` VARCHAR(300) NULL,
  `Email` VARCHAR(150) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`Order` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Address` VARCHAR(300) NULL,
  `Amount` INT NULL,
  `Created` DATETIME NULL,
  `Status` INT NULL,
  `User_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Order_User_idx` (`User_Id` ASC),
  CONSTRAINT `fk_Order_User`
    FOREIGN KEY (`User_Id`)
    REFERENCES `mydb`.`User` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`Category` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NULL,
  `Image` VARCHAR(45) NULL,
  `Active` TINYINT NULL,
  `Created` DATETIME NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`Product` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NULL,
  `Description` VARCHAR(500) NULL,
  `Code` VARCHAR(45) NULL,
  `Views` BIGINT NULL,
  `Active` TINYINT NULL,
  `Created` DATETIME NULL,
  `Updated` DATETIME NULL,
  `Category_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_Product_Category1_idx` (`Category_Id` ASC),
  CONSTRAINT `fk_Product_Category1`
    FOREIGN KEY (`Category_Id`)
    REFERENCES `mydb`.`Category` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Order_has_Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`Order_has_Product` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Order_Id` INT NOT NULL,
  `Product_Id` INT NOT NULL,
   PRIMARY KEY (`Id`),
  INDEX `fk_Order_has_Product_Product1_idx` (`Product_Id` ASC),
  INDEX `fk_Order_has_Product_Order1_idx` (`Order_Id` ASC),
  CONSTRAINT `fk_Order_has_Product_Order1`
    FOREIGN KEY (`Order_Id`)
    REFERENCES `mydb`.`Order` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_has_Product_Product1`
    FOREIGN KEY (`Product_Id`)
    REFERENCES `mydb`.`Product` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


ALTER TABLE `shop`.`Product` ADD Price Double NULL; 



