package tests;

import daos.CategoryDao;
import daos.ProductDao;
import daos.UserDao;
import entities.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utilities.DbAccessException;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ProductDaoTest {
    @BeforeEach
    void setUp() {

    }

    @Test
    void getEntityName() throws DbAccessException {

      /*  UserDao d = new UserDao();
        User u = new User();
        u.setName("Ivan");
        d.save(u);*/

       // CategoryDao categoryDao = new CategoryDao();
        Category category = new Category();
        category.setName("Mouses");
        category.setActive((byte)1);
        category.setCreated(new Timestamp(System.currentTimeMillis()));
        category.setId(1);
        //categoryDao.save(category);

        ProductDao dao = new ProductDao();
        Product p = new Product();
        p.setName("Water");
        p.setPrice(10.5);
        p.setDescription("");
        p.setCategory(category);

        p.setUpdated(new Timestamp(System.currentTimeMillis()));
        dao.save(p);

        List<Product> productList = dao.getAll();
        assertTrue(true);
    }
}