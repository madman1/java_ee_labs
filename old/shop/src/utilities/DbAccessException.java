package utilities;

public class DbAccessException extends Exception {
    public DbAccessException() { super(); }
    public DbAccessException(String message) { super(message); }
    public DbAccessException(String message, Throwable cause) { super(message, cause); }
    public DbAccessException(Throwable cause) { super(cause); }
}

