package daos;
import entities.*;

public class ProductDao extends BaseDao<Product>
{
    @Override
    protected String getEntityName() {
        return "Product";
    }

    @Override
    protected Class getEntityClass() {
        return Product.class;
    }
}
