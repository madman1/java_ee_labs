package daos;
import entities.*;

public class UserDao extends BaseDao<User>
{
    @Override
    protected String getEntityName() {
        return "User";
    }

    @Override
    protected Class getEntityClass() {
        return User.class;
    }
}