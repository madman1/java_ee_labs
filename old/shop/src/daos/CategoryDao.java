package daos;

import entities.*;

public class CategoryDao extends BaseDao<Category>
{
    @Override
    protected String getEntityName() {
        return "Category";
    }

    @Override
    protected Class getEntityClass() {
        return Category.class;
    }
}