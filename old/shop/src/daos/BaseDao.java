package daos;

import utilities.DbAccessException;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

public abstract class BaseDao<T>
{
    protected Class entityClass;
    protected String entityName;

    protected EntityManager entityManager;

    public BaseDao()
    {
        entityClass = getEntityClass();
        entityName = getEntityName();
        EntityManagerFactory factory  = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        entityManager = factory.createEntityManager();
    }

   /*@PersistenceContext
    public void setEntityManager(final EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }
*/
    protected abstract String getEntityName();
    protected abstract Class getEntityClass();

    public List<T> getAll() throws DbAccessException
    {
        try {
            String query = "select c from " + entityName +" c";
            List<T> customers = entityManager.createQuery(query, entityClass).getResultList();
            return customers;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while retreiving objects from db");
        }
    }

    //@Transactional()
    public T getEntityById(int id) throws DbAccessException
    {
        try
        {
            T obj = (T)entityManager.find(entityClass,id);
            return (T)obj;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while retreiving object from db. Id=" + id);
        }
    }

    @Transactional
    public void delete(int id) throws DbAccessException
    {
        try
        {
            T obj = getEntityById(id);
            entityManager.remove(obj);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while deleting of object from db. Id=" + id);
        }
    }

   // @Transactional
    public void save(T entity) throws DbAccessException
    {
        try
        {
            entityManager.getTransaction().begin();
            entityManager.merge(entity);
            entityManager.getTransaction().commit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new DbAccessException("Exception appeared while inserting of object into db" + entity);
        }
    }
}