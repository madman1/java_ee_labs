package controllers;

import daos.ProductDao;
import entities.*;
import entities.requests.ProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import utilities.DbAccessException;

import java.sql.Timestamp;
import java.util.List;

@Controller
public class ProductRestController
{
    @Autowired
    private ProductDao productDao;

    @RequestMapping(value = "/product", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Product> getAll() throws DbAccessException
    {
        return productDao.getAll();
    }

    //@RequestParam
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Product getById(@PathVariable int id) throws DbAccessException
    {
        return productDao.getEntityById(id);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public void deleteById(@PathVariable int id) throws DbAccessException
    {
        productDao.delete(id);
    }

    @RequestMapping(value = "/product", method = RequestMethod.POST, produces = "application/json")
    public void createNew(@RequestBody ProductRequest newProduct) throws DbAccessException
    {
        Product res = new Product();
        res.setDescription(newProduct.getDescription());

        res.setUpdated(new Timestamp(newProduct.getLastUpdate()));
        res.setName(newProduct.getName());
        res.setPrice(newProduct.getPrice());
        res.setActive(newProduct.isActive()? (byte)1 : (byte)0);
        productDao.save(res);
    }

    @RequestMapping(value = "/product", method = RequestMethod.PUT, produces = "application/json")
    public void update(@RequestBody ProductRequest product) throws DbAccessException
    {
        Product res = new Product();
        res.setDescription(product.getDescription());
        res.setUpdated(new Timestamp(product.getLastUpdate()));
        res.setName(product.getName());
        res.setPrice(product.getPrice());
        res.setActive(product.isActive()? (byte)1 : (byte)0);
        res.setId(product.getId());
        productDao.save(res);
    }
}
